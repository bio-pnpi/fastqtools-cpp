/*
 * This file is part of the fastqtools-cpp distribution (https://gitlab.com/bio-pnpi/fastqtools-cpp).
 * Copyright (c) 2023 Alexey Shvetsov
 * Copyright (c) 2023 Anna Tsvetkova
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef FASTQTOOLS_SEARCHER_H
#define FASTQTOOLS_SEARCHER_H


#include <string>
#include <set>


struct Interval {
    size_t begin = 0;
    size_t end = 0;
    int error = -1;

    bool operator<(const Interval &rhs) const {
        return (begin < rhs.begin)
               || ((begin == rhs.begin) && (end < rhs.end));
    }

};


class Searcher {
public:
    void setSubSequence(const std::string &subsequence);

    void setSequence(const std::string &sequence);

    void setPercentCoarseSearch(unsigned int percent);
    void setPercentFineSearch(unsigned int percent);

    std::set<Interval> getResults();

private:
    std::string subsequence_;
    std::string sequence_;
    unsigned int percentCoarseSearch_ = 70;
    unsigned int percentFineSearch_ = 33;
    std::set<Interval> intervals_;

    static int damerauLevenshteinDistance(std::string p_string1, std::string p_string2, size_t max_errors);

    static unsigned int calculateAmountOfParts(const std::string &string_1, const std::string &string_2);

    void getSearchIntervals();

    void analyzeIntervals(int percent_of_errors);

    void extendIntervals();

    void printIntervals();


};


#endif //FASTQTOOLS_SEARCHER_H
