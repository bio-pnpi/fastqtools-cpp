/*
 * This file is part of the fastqtools-cpp distribution (https://gitlab.com/bio-pnpi/fastqtools-cpp).
 * Copyright (c) 2023 Alexey Shvetsov
 * Copyright (c) 2023 Anna Tsvetkova
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef FASTQTOOLS_CPP_READFASTQ_H
#define FASTQTOOLS_CPP_READFASTQ_H


#include <vector>
#include <string>

#include "FastQRecord.h"

class FastQIO {

public:
    void printrecords();
    void read(const std::string &fnm);
    void write(const std::string &fnm);
    void printrecord(std::size_t id);

    std::vector<FastQRecord> getRecords();
    void setRecords(std::vector<FastQRecord> &records);

private:
    std::vector<FastQRecord> records_;
};


#endif //FASTQTOOLS_CPP_READFASTQ_H
