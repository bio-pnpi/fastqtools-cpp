/*
 * This file is part of the fastqtools-cpp distribution (https://gitlab.com/bio-pnpi/fastqtools-cpp).
 * Copyright (c) 2023 Alexey Shvetsov
 * Copyright (c) 2023 Anna Tsvetkova
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

#include <iostream>
#include <cmath>
#include "Searcher.h"

void Searcher::setSubSequence(const std::string &subsequence) {
    subsequence_ = subsequence;
}

void Searcher::setSequence(const std::string &sequence) {
    sequence_ = sequence;
}

std::set<Interval> Searcher::getResults() {
    getSearchIntervals();
    analyzeIntervals(percentCoarseSearch_);
    extendIntervals();
    analyzeIntervals(percentFineSearch_);
    return intervals_;

}

int Searcher::damerauLevenshteinDistance(std::string p_string1, std::string p_string2, size_t max_errors) {
    size_t length_1 = p_string1.length();
    size_t length_2 = p_string2.length();
    size_t d[length_1 + 1][length_1 + 1];

    size_t cost;

    for (auto i = 0; i <= length_1; i++) {
        d[i][0] = i;
    }
    for (auto j = 0; j <= length_2; j++) {
        d[0][j] = j;
    }
    for (auto i = 1; i <= length_1; i++) {
        for (auto j = 1; j <= length_2; j++) {
            if (p_string1[i - 1] == p_string2[j - 1])
                cost = 0;
            else
                cost = 1;

            d[i][j] = std::min(
                    d[i - 1][j] + 1,                  // delete
                    std::min(d[i][j - 1] + 1,         // insert
                             d[i - 1][j - 1] + cost)  // substitution
            );
            if ((i > 1) &&
                (j > 1) &&
                (p_string1[i - 1] == p_string2[j - 2]) &&
                (p_string1[i - 2] == p_string2[j - 1])) {
                d[i][j] = std::min(
                        d[i][j],
                        d[i - 2][j - 2] + cost);   // transposition
            }
        }
    }
    if (d[length_1][length_2] <= max_errors)
        return static_cast<int>(d[length_1][length_2]);
    else return -1;

}

unsigned int Searcher::calculateAmountOfParts(const std::string &string_1, const std::string &string_2) {
    unsigned int amount_of_parts;
    if (string_2.length() % string_1.length() == 0) {
        amount_of_parts = string_2.length() / string_1.length();
    } else
        amount_of_parts = std::ceil(static_cast<double>(string_2.length()) / static_cast<double>(string_1.length()));
    return static_cast<unsigned int>(amount_of_parts);

}

void Searcher::getSearchIntervals() {
    Interval interval;
    size_t length = subsequence_.length();
    size_t amount_of_parts = calculateAmountOfParts(subsequence_, sequence_);
    for (auto i = 0; i < amount_of_parts; i++) {
        interval.begin = i * length;
        interval.end = (i + 1) * length;
        if (interval.end > sequence_.length()) {
            interval.end = sequence_.length() - 1;
            interval.begin = interval.end - length;
        };
        intervals_.insert(interval);
    };

}

void Searcher::analyzeIntervals(int percent_of_errors) {
    size_t max_errors_for_parts = percent_of_errors * subsequence_.length() / 100;
    std::set<Interval> good_intervals;

    for (auto &interval: intervals_) {
        Interval iint;
        iint.begin = interval.begin;
        iint.end = interval.end;
        iint.error = damerauLevenshteinDistance(subsequence_, sequence_.substr(iint.begin,
                                                                               iint.end - iint.begin),
                                                max_errors_for_parts);
        if (iint.error != -1) {
            good_intervals.insert(iint);
        }
    }
    intervals_.clear();
    for (auto &interval: good_intervals) {
        intervals_.insert(interval);
    }

}

void Searcher::extendIntervals() {
    std::set<Interval> intervals;
    for (auto &interval: intervals_) {
        Interval itmp;
        if (interval.begin > subsequence_.length()) {
            itmp.begin = interval.begin - subsequence_.length();
        } else {
            itmp.begin = 0;
        }
        if (interval.end + subsequence_.length() < sequence_.length()) {
            itmp.end = interval.end + subsequence_.length();
        } else {
            itmp.end = sequence_.length();
        }
        intervals.insert(itmp);
    }
    intervals_.clear();
    for (auto &interval: intervals) {
        intervals_.insert(interval);
    }
    intervals.clear();
    for (auto &interval: intervals_) {
        for (size_t i = 0; i < (interval.end - interval.begin - subsequence_.length()); i++) {
            Interval icurrent;
            icurrent.begin = interval.begin + i;
            icurrent.end = interval.begin + i + subsequence_.length();
            intervals.insert(icurrent);
        }
    }
    intervals_.clear();
    for (auto &interval: intervals) {
        intervals_.insert(interval);
    }

}

void Searcher::printIntervals() {
    std::cout << "Intervals are (format [begin, end, error]):\n";
    for (auto &interval: intervals_) {
        std::cout << "[ " << interval.begin << ", " << interval.end << ", " << interval.error << " ]" << std::endl;
    }

}

void Searcher::setPercentCoarseSearch(unsigned int percent) {
    percentCoarseSearch_ = percent;
}

void Searcher::setPercentFineSearch(unsigned int percent) {
    percentFineSearch_ = percent;
}

