/*
 * This file is part of the fastqtools-cpp distribution (https://gitlab.com/bio-pnpi/fastqtools-cpp).
 * Copyright (c) 2023 Alexey Shvetsov
 * Copyright (c) 2023 Anna Tsvetkova
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

#include "seqTools.h"

seqTools::seqTools(std::string seq) {
    seq_ = seq;
}

std::string seqTools::reverse() {
    std::reverse(seq_.begin(), seq_.end());
    return seq_;
}

std::string seqTools::compliment() {
    for (char & i : seq_){
        switch (i){
            case 'A':
                i = 'T';
                break;
            case 'C':
                i = 'G';
                break;
            case 'G':
                i = 'C';
                break;
            case 'T':
                i = 'A';
                break;
        }
    }
    return seq_;
}

std::string seqTools::reversecompliment() {
    reverse();
    compliment();
    return seq_;
}
