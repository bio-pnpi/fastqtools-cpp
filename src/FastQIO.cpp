/*
 * This file is part of the fastqtools-cpp distribution (https://gitlab.com/bio-pnpi/fastqtools-cpp).
 * Copyright (c) 2023 Alexey Shvetsov
 * Copyright (c) 2023 Anna Tsvetkova
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

#include <iostream>
#include <fstream>
#include <utility>
#include "FastQIO.h"


void FastQIO::printrecords() {
    for (auto &record: records_) {
        std::cout
                << "@" << record.uuid << std::endl
                << record.sequence << std::endl
                << "+" << record.id << std::endl
                << record.quality << std::endl;
    }
}

void FastQIO::printrecord(std::size_t id) {
    std::cout
            << "@" << records_[id].uuid << std::endl
            << records_[id].sequence << std::endl
            << "+" << records_[id].id << std::endl
            << records_[id].quality << std::endl;
}

std::vector<FastQRecord> FastQIO::getRecords() {
    return records_;
}

void FastQIO::read(const std::string &fnm) {
    std::fstream fastqfd;
    std::vector<std::string> lines;
    fastqfd.open(fnm, std::ios::in);
    if (fastqfd.is_open()) {
        std::string line;
        while (std::getline(fastqfd, line)) {
            lines.push_back(line);
        }
    }

    for (auto i = 0; i < lines.size() / 4; ++i) {
        FastQRecord record;
        std::string uuid = lines[4 * i + 0];
        std::string sequence = lines[4 * i + 1];
        std::string id = lines[4 * i + 2];
        std::string quality = lines[4 * i + 3];
        char at = '@';
        if (uuid.front() == at) {
            uuid.erase(uuid.begin());
        }
        char plus = '+';
        if (id.front() == plus) {
            id.erase(id.begin());
        }
        record.uuid = uuid;
        record.sequence = sequence;
        record.id = id;
        record.quality = quality;
        record.len = sequence.size();
        records_.push_back(record);
    }
    fastqfd.close();
    std::cout << "Read " << records_.size() << " fastq records" << std::endl;
}

void FastQIO::write(const std::string &fnm) {
    std::fstream fastqfd;
    fastqfd.open(fnm, std::ios::out);
    if (fastqfd.is_open()) {
        for (auto &record: records_) {
            fastqfd
                    << "@" << record.uuid << std::endl
                    << record.sequence << std::endl
                    << "+" << record.id << std::endl
                    << record.quality << std::endl;
        }
    }
    fastqfd.close();
}

void FastQIO::setRecords(std::vector<FastQRecord> &records) {
    records_ = records;
}

