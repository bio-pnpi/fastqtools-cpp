/*
 * This file is part of the fastqtools-cpp distribution (https://gitlab.com/bio-pnpi/fastqtools-cpp).
 * Copyright (c) 2023 Alexey Shvetsov
 * Copyright (c) 2023 Anna Tsvetkova
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

#include <iostream>
#include <fstream>
#include <string>
#include <vector>
#include <argparse/argparse.hpp>
#include <regex>
#include <omp.h>

#include "FastQIO.h"
#include "seqTools.h"
#include "Searcher.h"

int main(int argc, char *argv[]) {
    argparse::ArgumentParser program("fastqtools");
    program.add_argument("-f", "--fastq").help("fastq file to read").required();
    program.add_argument("-b", "--barcode").help("barcodes to search");
    program.add_argument("--coarse-search-error").default_value(70).scan<'i', std::size_t>().help("Error % for coarse search");
    program.add_argument("--fine-search-error").default_value(5).scan<'i', std::size_t>().help("Error % for fine search");
    try {
        program.parse_args(argc, argv);
    }
    catch (const std::runtime_error &err) {
        std::cerr << err.what() << std::endl;
        std::cerr << program;
        std::exit(1);
    }

    auto fastqfnm = program.get<std::string>("--fastq");
    //size_t coarseSearchError = program.get<std::size_t>("--coarse-search-error");
    //size_t fineSearchError = program.get<std::size_t>("--fine-search-error");
    FastQIO fastQio;

    fastQio.read(fastqfnm);

    std::string barcodeF = "GACTACTTTCTGCCTTTGCGAGAA";
    std::string barcodeR = seqTools(barcodeF).reversecompliment();

    std::vector<FastQRecord> records = fastQio.getRecords();

    size_t hasBoth = 0, hasForwardOnly = 0, hasReverseOnly = 0, hasNone = 0;

    std::vector<FastQRecord> recordsHasBoth;
    std::vector<FastQRecord> recordsHasNone;
    std::vector<FastQRecord> recordsHasForwardOnly;
    std::vector<FastQRecord> recordsHasReverseOnly;
#pragma omp parallel default(none) shared(records, barcodeF, barcodeR, hasNone, hasReverseOnly, hasForwardOnly, hasBoth, recordsHasBoth, recordsHasNone, recordsHasReverseOnly, recordsHasForwardOnly, std::cout)
    {
        std::vector<FastQRecord> recordsHasBothThr;
        std::vector<FastQRecord> recordsHasNoneThr;
        std::vector<FastQRecord> recordsHasForwardOnlyThr;
        std::vector<FastQRecord> recordsHasReverseOnlyThr;
#pragma omp for nowait reduction(+:hasBoth, hasForwardOnly, hasReverseOnly, hasNone)
        for (auto &record: records) {
            Searcher searcher;
            //searcher.setPercentCoarseSearch(coarseSearchError);
            //searcher.setPercentFineSearch(fineSearchError);
            searcher.setSequence(record.sequence);
            searcher.setSubSequence(barcodeF);
            std::set<Interval> resultF = searcher.getResults();
            searcher.setSubSequence(barcodeR);
            std::set<Interval> resultR = searcher.getResults();
            if (resultF.empty() && resultR.empty()) {
                hasNone += 1;
                recordsHasNoneThr.push_back(record);
            } else if (resultF.empty() && !resultR.empty()) {
                hasReverseOnly += 1;
                recordsHasReverseOnlyThr.push_back(record);
            } else if (!resultF.empty() && resultR.empty()) {
                hasForwardOnly += 1;
                recordsHasForwardOnlyThr.push_back(record);
            } else {
                hasBoth += 1;
                recordsHasBothThr.push_back(record);
            }
            std::cout
                << "Sequence: " << record.sequence << std::endl
                << "BarcodeF: " << barcodeF << " Pos: ";
            if (!resultF.empty()) {
                for (auto &pos: resultF) {
                    std::cout << "[ " << pos.begin << ", " << pos.end << ", " << pos.error << " ] ";
                }
                std::cout << std::endl;
            } else {
                std::cout << "None " << std::endl ;
            }
            std::cout
                << "BarcodeR: " << barcodeR << " Pos: ";
            if (!resultR.empty()) {
                for (auto &pos: resultR) {
                    std::cout << "[ " << pos.begin << ", " << pos.end << ", " << pos.error << " ] ";
                }
                std::cout << std::endl;
            } else {
                std::cout << "None " << std::endl ;
            }
        }
#pragma omp critical
        {
            recordsHasBoth.insert(recordsHasBoth.end(),
                                  std::make_move_iterator(recordsHasBothThr.begin()),
                                  std::make_move_iterator(recordsHasBothThr.end()));
            recordsHasNone.insert(recordsHasNone.end(),
                                  std::make_move_iterator(recordsHasNoneThr.begin()),
                                  std::make_move_iterator(recordsHasNoneThr.end()));
            recordsHasForwardOnly.insert(recordsHasForwardOnly.end(),
                                         std::make_move_iterator(recordsHasForwardOnlyThr.begin()),
                                         std::make_move_iterator(recordsHasForwardOnlyThr.end()));
            recordsHasReverseOnly.insert(recordsHasReverseOnly.end(),
                                         std::make_move_iterator(recordsHasReverseOnlyThr.begin()),
                                         std::make_move_iterator(recordsHasReverseOnlyThr.end()));
        }
    }

    fastQio.setRecords(recordsHasNone);
    fastQio.write(std::regex_replace(fastqfnm, std::regex(".fastq"), "-hasNone.fastq"));
    fastQio.setRecords(recordsHasBoth);
    fastQio.write(std::regex_replace(fastqfnm, std::regex(".fastq"), "-hasBoth.fastq"));
    fastQio.setRecords(recordsHasForwardOnly);
    fastQio.write(std::regex_replace(fastqfnm, std::regex(".fastq"), "-hasForwardOnly.fastq"));
    fastQio.setRecords(recordsHasReverseOnly);
    fastQio.write(std::regex_replace(fastqfnm, std::regex(".fastq"), "-hasReverseOnly.fastq"));

    std::cout
            << "Records        : " << records.size() << std::endl
            << "hasBoth        : " << hasBoth        << std::endl
            << "hasForwardOnly : " << hasForwardOnly << std::endl
            << "hasReverseOnly : " << hasReverseOnly << std::endl
            << "hasNone        : " << hasNone        << std::endl;


    return 0;
}
